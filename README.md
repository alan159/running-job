running-job
|-- node_modules
|-- public
    |--favicon.ico 浏览器标识
    |--index.html: 主页面文件
|-- src : 源码文件夹
    |--api 与后台交互模块文件夹
    |--assets 静态资源img、css、js，也可放在相对路由目录下
    |--components 非路由组件
    |--filters 自定义过滤器模块文件夹
    |--plugins 插件
    |--router 路由文件夹
    |--store vuex文件夹
    |--views 路由组件
    |--main.js: 应用入口 js
|--.browserslistrc 目标浏览器配置表
|-- .editorconfig: 通过编辑器的编码/格式进行一定的配置
|-- .eslintignore: eslint 检查忽略的配置
|-- .eslintrc.js: eslint 检查的配置
|-- .gitignore: git 版本管制忽略的配置
|-- .babel.config.js: babel 的配置文件
|-- package.json: 应用包配置文件
|-- README.md: 应用描述说明的 readme 文件
# 启动方式
npm run serve
