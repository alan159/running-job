import request from "@/utils/request";
export function jobsInit(page = 1, size = 10) {
  return request({
    url: "/resources-action/jobs",
    params: {
      page: page - 1,
      size: size
    }
  });
}
export function getJobTags() {
  return request({
    url: "/resources-action/jobs/tags"
  });
}
export function searchJobByTag(tag, page = 1, size = 10) {
  return request({
    url: "/resources-action/jobs/search_by_tag_pretty",
    params: {
      keyword: tag,
      page: page - 1,
      size: size
    }
  });
}
export function workDetail(id) {
  return request({
    url: "/resources-action/jobs/" + id
  });
}

export function addWork(form) {
  return request({
    url: "/resources-action/jobs",
    data: form,
    method: "post"
  });
}

export function lookMyWork(page, size) {
  return request({
    url: "/resources-action/jobs/published",
    params: {
      page: page - 1,
      size: size
    }
  });
}
