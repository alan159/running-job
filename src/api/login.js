import request from "@/utils/request";
export function reqpwdLogin(account, password) {
  return request({
    url: "/oauth/token",
    method: "post",
    data: {
      grant_type: "password",
      username: account,
      password: password,
      client_id: "website",
      client_secret: "string",
      code: "string",
      redirect_uri: "string"
    }
  });
}
export function login_bycaptcha(account, captcha, captcha_id) {
  return request({
    url: "/oauth/token",
    method: "post",
    headers: { "captcha-id": captcha_id },
    data: {
      grant_type: "sms_code",
      username: account,
      client_id: "website",
      password: "string" ,
      client_secret: "string",
      code: captcha,
      redirect_uri: "string"
    }
  });
}
export function sms(phone_number) {
  return request({
    url: "/app-captcha/sms",
    headers: { "Captcha-Id": "0000" },
    params: {
      img_text: "0000",
      debug: true,
      phone_number: phone_number
    }
  });
}

export function email(email_address) {
  return request({
    url: "/app-captcha/email",
    headers: { "Captcha-Id": "0000" },
    params: {
      img_text: "0000",
      email_address: email_address
    }
  });
}
export function registerUserByPhone(
  phone_number,
  password,
  username,
  captcha_text,
  captcha_id
) {
  return request({
    url: "/reg/user",
    headers: { "captcha-id": captcha_id },
    data: {
      phone_number: phone_number,
      password: password,
      username: username,
      captcha_text: captcha_text
    },
    method: "post"
  });
}
export function registerUserByEmail(
  email,
  password,
  username,
  captcha_text,
  captcha_id
) {
  return request({
    url: "/reg/user",
    headers: { "captcha-id": captcha_id },
    data: {
      email: email,
      password: password,
      username: username,
      captcha_text: captcha_text
    },
    method: "post"
  });
}
export function test_account_phone(phone_number) {
  return request({
    url: "/reg/valid/suppress_xhr_error",
    data: {
      phone_number: phone_number
    },
    method: "post"
  });
}
export function test_account_email(email) {
  return request({
    url: "/reg/valid/suppress_xhr_error",
    data: {
      email: email
    },
    method: "post"
  });
}
export function test_account_username(username) {
  return request({
    url: "/reg/valid/suppress_xhr_error",
    data: {
      username: username
    },
    method: "post"
  });
}
export function forget_user_byphone(
  phone_number,
  password,
  captcha_text,
  captcha_id
) {
  return request({
    url:'/reg/reset',
    headers: { "Captcha-Id": captcha_id },
    data:{
      phone_number: phone_number,
      password: password,
      captcha_text:captcha_text
    },
    method:'post'
  })
}
export function forget_user_byemail(
  email,
  password,
  captcha_text,
  captcha_id
) {
  return request({
    url:'/reg/reset',
    headers: { "Captcha-Id": captcha_id },
    data:{
      email: email,
      password: password,
      captcha_text:captcha_text
    },
    method:'post'
  })
}