import request from "@/utils/request";
export function login_out() {
  return request({
    url: "/resources-action/token/logout"
  });
}
export function get_info() {
  return request({
    url: "/resources-action/user/self"
  });
}
export function editSelf(
  username = null,
  avatar = null,
  gender = null,
  real_name = null,
  wechat_number = null,
  qq_number = null,
  class_name = null,
  common_name = null,
  department = null,
  description = null,
  email = null,
  phone_number = null,
  captcha = null
) {
  return request({
    url: "/resources-action/user/self/edit",
    data: {
      username: username,
      avatar: avatar,
      gender: gender,
      real_name: real_name,
      captcha: captcha,
      phone_number: phone_number,
      qq_number: qq_number,
      description: description,
      class_name: class_name,
      common_name: common_name,
      department: department,
      wechat_number: wechat_number,
      email: email
    },
    method: "patch"
  });
}
export function editSelfSafe(captchaId,captcha,captcha_dest_addr,password,phoneNumber,email) {
  return request({
    url: "/resources-action/user/self/edit",
    headers: { "captcha-id": captchaId },
    data:{
      password:password || null,
      phone_number:phoneNumber || null,
      email:email || null,
      captcha_text:captcha,
      captcha_dest_addr:captcha_dest_addr
    },
    method: "patch"
  });
}
export function CooperationApply(form) {
  return request({
    url:'/resources-action/apply',
    data:form,
    method:'post'
  })
}
export function cooperationing(){
  return request({
    url:'/resources-action/apply',
    params:{
      page:0,
      size:20
    }
  })
}