import request from "@/utils/request";
export function search_work(
  keyword,
  page = 0,
  size = 20,
  lon = null,
  lat = null
) {
  return request({
    url: "/resources-action/jobs/search_by_tag",
    params: {
      keyword: keyword,
      page: page,
      size: size,
      lon: lon,
      lat: lat
    }
  });
}
export function search_tag(){
    return request({
        url:"/resources-action/jobs/tags"
    })
}