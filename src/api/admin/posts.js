import request from "@/utils/request.js";

//获得全部帖子
export function init(page, size) {
    return request({
        url: "/resources-action/posts",
        method: "get",
        params: {
            page,
            size,
            sort: "created_time,desc"
        }
    })
}

//删除帖子
export function delPosts(post_id) {
    return request({
        url: `/resources-action/posts/${post_id}`,
        method: 'delete',
        data: ""
    })
}

//渲染完整帖子
export function renderPosts(post_id) {
    return request({
        url: `/resources-action/posts/${post_id}/fully_rendered`,
        method: 'get',
        params: ""
    })
}

//根据条件分页
export function init_Term(url, params) {
    return request({
        url,
        method: 'get',
        params
    })
}

//根据模块查找帖子
export function getPlatePosts(plate_id, page, size) {
    return request({
        url: `/resources-action/posts/plate/${plate_id}`,
        method: 'get',
        params: {
            page,
            size,
            sort: "created_time"
        }
    })
}

//任意字段搜索帖子
export function searchPosts(keyword, size) {
    console.log(keyword);
    return request({
        url: "/resources-action/posts/search_pretty",
        method: 'get',
        params: {
            keyword,
            page: 0,
            size,
        }
    })
}

//获取所有板块
export function getPlate(page, size) {
    return request({
        url: "/resources-action/plates",
        method: 'get',
        params: {
            page,
            size,
            sort: "created_time,desc"
        }
    })
}

//添加板块
export function addPlate(topic) {
    return request({
        url: "/resources-action/plates",
        method: 'post',
        data: {
            topic
        }
    })
}

//删除板块
export function delPlate(plate_id) {
    return request({
        url: `/resources-action/plates/${plate_id}`,
        method: 'delete',
        params: ""
    })
}