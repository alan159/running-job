// 首页
import request from "@/utils/request.js";

export function getAll() {
    return request({
        url: "/resources-action/statistic/all",
        method: "get",
        params: ""
    })
}