import request from "@/utils/request.js";

//获取所有Jobs
export function init(page, size) {
    return request({
        url: "/resources-action/jobs",
        method: "get",
        params: {
            page,
            size,
            sort: "created_time,desc"
        }
    })
}

//删除Job
export function deleteJob(job_id) {
    return request({
        url: `/resources-action/jobs/${job_id}`,
        method: "DELETE",
        data: '',
    })
}

//搜索Job
export function searchJob(keyword, page, size) {
    return request({
        url: `/resources-action/jobs/search`,
        method: "get",
        params: {
            keyword,
            page,
            size
        },
    })
}

//根据条件分页查询
export function init_condition(url, params) {
    return request({
        url,
        method: "get",
        params
    })
}