import request from '@/utils/request'

//初始化数据
export function init(page, size) {
    return request({
        url: "/resources-action/apply/pull",
        method: "get",
        params: {
            page,
            size,
        }
    });
}

//查询数据（成功/失败）
export function init_history(pass, page, size) {
    return request({
        url: "/resources-action/apply/history",
        method: "get",
        params: {
            pass,
            page,
            size,
        }
    });
}

//驳回申请
export function denyFirm(id, reason) {
    return request({
        url: `/resources-action/apply/${id}/deny`,
        method: "post",
        data: {
            reason
        }
    });
}

//批准申请
export function passFirm(id) {
    return request({
        url: `/resources-action/apply/${id}/pass`,
        method: "post",
        data: ''
    });
}