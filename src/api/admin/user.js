import request from '@/utils/request'

//初始化数据
export function init(page, size) {
    return request({
        url: "/resources-action/user",
        method: "get",
        params: {
            page,
            size,
            sort: "registerTime,desc"
        }
    });
}

//模糊查询用户
export function userSearch(key, val, page, size) {
    let params = {};
    params[key] = val;
    params.page = page;
    params.size = size;
    params.sort = "registerTime,desc";
    return request({
        url: "/resources-action/user/search",
        method: "get",
        params
    })
}

//根据查询不同结果进行分页处理
export function init_page(url, page, size, key, val) {
    let params = {};
    params[key] = val;
    params.page = page;
    params.size = size;
    params.sort = "registerTime,desc";
    return request({
        url,
        method: 'get',
        params,
    });
}

//删除用户
export function delete_User(user_id) {
    return request({
        url: `/resources-action/user/${user_id}`,
        method: 'delete',
        data: '',
    });
}

//添加用户
export function newUser(user) {
    return request({
        url: "/resources-action/user",
        method: "post",
        data: user,
    });
}

//修改用户
export function changeUser(user_id, user) {
    return request({
        url: `/resources-action/user/${user_id}`,
        method: "patch",
        data: user,
    });
}

//查询单个用户
export function getUser(user_id) {
    return request({
        url: `/resources-action/user/${user_id}`,
        method: "get",
        params: '',
    });
}

//获取当前登录用户信息
export function getUserSelf() {
    return request({
        url: '/resources-action/user/self',
        method: 'get',
        params: ''
    })
}

//判断用户是否存在
export function isContains(data) {
    return request({
        url: '/reg/valid/suppress_xhr_error',
        method: 'post',
        data
    })
}