import {
    init,
    delPosts,
    init_Term,
    searchPosts,
    getPlate,
    getPlatePosts
} from "@/api/admin/posts.js";

const state = {
    tableData: [{
        publisher: {
            username: "",
            wechat_number: "",
        },
        created_time: "",
        title: "",
        plate: "",
        content: "",
        root: true,
    }, ],
    page: {},
    url_data: {},
    //模糊搜索表单
    ruleForm: {
        keyword: "",
        plates: [{
            // id:"111",//显示出来的文字
            // topic:"222"
        }],
        value: "",
    },
}

const getters = {}

const mutations = {
    TERM_DATA(state, { response }) {
        this.commit('adminPosts/RESET_FORM', { key: "value" });
        state.url_data.url = response.config.url;
        state.url_data.params = response.config.params;
        const { data } = response;
        state.tableData = [];
        for (const key of data.content) {
            state.tableData.push(key.content);
        }
        state.page.total_elements = data.total_elements;
        state.page.number = data.number;
        state.page.size = data.size;
    },
    NORMAL_DATA(state, { response }) {
        this.commit('adminPosts/RESET_FORM', { key: "keyword" });
        state.url_data.url = response.config.url;
        state.url_data.params = response.config.params;
        const { data } = response;
        state.tableData = data._embedded ? data._embedded.posts : [];
        state.page = data.page ? data.page : {};
    },
    IS_DATA(state, { response }) {
        if (state.url_data.params.keyword) {
            this.commit('adminPosts/TERM_DATA', { response });
        } else {
            this.commit('adminPosts/NORMAL_DATA', { response });
        }
    },
    GET_PLATE(state, { data }) {
        state.ruleForm.plates = data._embedded.plates;
    },
    HANDLE_SIZE_CHANGE(state, { val }) {
        let count = state.page.total_elements;
        let page = state.page.number;
        page = page > Math.floor(count / val) ? Math.floor(count / val) : page;
        state.url_data.params.page = page;
        state.url_data.params.size = val;
    },
    HANDLE_CURRENT_CHANGE(state, { val }) {
        state.url_data.params.page = val - 1;
    },
    RESET_FORM(state, { key }) {
        state.ruleForm[key] = "";
    }
}

const actions = {
    //初始化获得所有帖子
    getData({ commit }, { page, size }) {
        return new Promise((resolve, reject) => {
            init(page, size)
                .then((response) => {
                    commit('NORMAL_DATA', { response });
                    resolve();
                })
                .catch((error) => {
                    reject(error);
                })
        })
    },
    //获取所有板块（无分页）
    getPlate({ commit }, { page, size }) {
        return new Promise((resolve, reject) => {
            getPlate(page, size)
                .then((response) => {
                    const { data } = response;
                    commit('GET_PLATE', { data });
                    resolve();
                })
                .catch(error => {
                    reject(error);
                })

        })

    },
    //根据板块查询帖子
    initPlatePosts({ commit }, { plate_id, page, size }) {
        return new Promise((resolve, reject) => {
            getPlatePosts(plate_id, page, size)
                .then((response) => {
                    commit('NORMAL_DATA', { response })
                    resolve();
                })
                .catch((error) => {
                    reject(error);
                })
        })
    },
    //根据条件得到数据
    getTermData({ commit }, { url, params }) {
        return new Promise((resolve, reject) => {
            init_Term(url, params)
                .then((response) => {
                    commit('IS_DATA', { response });
                    resolve();
                })
                .catch((error) => {
                    reject(error);
                })
        })
    },
    //按照任意字段查找帖子
    findPosts({ commit }, { keyword, size }) {
        console.log(keyword, size);
        return new Promise((resolve, reject) => {
            searchPosts(keyword, size)
                .then((response) => {
                    commit('TERM_DATA', { response });
                    resolve();
                })
                .catch((error) => {
                    reject(error);
                })
        })
    },
    //删除帖子
    removePosts({ commit }, { post_id }) {
        return new Promise((resolve, reject) => {
            delPosts(post_id)
                .then(response => {
                    const { status } = response;
                    resolve(status);
                })
                .catch(error => {
                    reject(error);
                })
        })
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}