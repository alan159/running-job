//引入api接口
import {
    init,
    userSearch,
    init_page,
    delete_User,
    newUser,
    getUser,
    changeUser,
    isContains
} from "@/api/admin/user.js";



//数据源
const state = {
    tableData: [], //表格数据
    page: {
        size: 0,
        number: 0,
        total_elements: 0
    }, //page数据
    change_user: {
        username: "",
        real_name: "",
        gender: "",
        age: "",
        authority: "",
        common_name: "",
        department: "",
        class_name: "",
        phone_number: "",
        qq_number: "",
        wechat_number: "",
        password: "",
        email: "",
        description: "",
    },
    url_data: {
        url: "",
    },
    formInline: {
        //查询信息表单
        username: "",
        common_name: "",
        authority: "",
        real_name: "",
    },
}

const getters = {
    // tableData: state => state.admin.tableData,
    // page: state => state.admin.page,
    // url_data: state => state.admin.url_data,
    // change_user: state => state.admin.change_user,
}


function getUrl(state, data) {
    state.tableData = data._embedded ? data._embedded.user_infoes : [];
    state.page = data.page ? data.page : {};
    let str = data._links.self.href
        .split("&")[0]
        .substring(data._links.self.href.split("&")[0].indexOf("/", 10));
    state.url_data.url = str.split("?")[0];
    return str;
}

//同步请求
const mutations = {
    GET_DATA: (state, { data }) => {
        getUrl(state, data);
        state.url_data.key = "";
        state.url_data.val = "";
    },
    FUZZY_SEARCH_USER: (state, { data }) => {
        let str = getUrl(state, data);
        state.url_data.key = str.split("?")[1].split("=")[0];
        state.url_data.val = str.split("?")[1].split("=")[1];
    },
    CHANGE_PAGE: (state, { data }) => {
        let str = getUrl(state, data);
        if (str.split("?")[1].split("=")[0] === "page") {
            state.url_data.key = "";
            state.url_data.val = "";
        } else {
            state.url_data.key = str.split("?")[1].split("=")[0];
            state.url_data.val = str.split("?")[1].split("=")[1];
        }
    },
    GAIN_USER: (state, { data }) => {
        state.change_user = data;
    },
    ADD_USER: (state, { data }) => {
        state.tableData.unshift(data);
    },
    CLEAR_USER: (state, { key, val }) => {
        state.formInline = {
            username: "",
            common_name: "",
            authority: "",
            real_name: "",
        }
        state.formInline[key] = val;
    },
}

//异步请求
const actions = {
    //查询用户信息
    getData({ commit }, { page, size }) {
        return new Promise((resolve, reject) => {
            init(page, size)
                .then(response => {
                    const { data } = response;
                    commit('GET_DATA', { data });
                    resolve();
                })
                .catch(error => {
                    reject(error);
                });
        });
    },
    //查询数据表单
    fuzzySearchUser({ commit }, { key, val, page, size }) {
        commit('CLEAR_USER', { key, val });
        return new Promise((resolve, reject) => {
            userSearch(key, val, page, size)
                .then((response) => {
                    const { data } = response;
                    commit('FUZZY_SEARCH_USER', { data });
                    resolve();
                })
                .catch((error) => {
                    reject(error);
                })

        })
    },
    //根据条件换页
    changePage({ commit }, { url, page, size, key, val }) {
        return new Promise((resolve, reject) => {
            init_page(url, page, size, key, val).then((response) => {
                const { data } = response;
                commit('CHANGE_PAGE', { data });
                resolve();
            }).catch(error => {
                reject(error);
            })

        })
    },
    //获取某个用户信息
    gainUser({ commit }, { id }) {
        return new Promise((resolve, reject) => {
            getUser(id).then((response) => {
                const { data } = response;
                commit('GAIN_USER', { data });
                resolve();
            }).catch(error => {
                reject(error);
            })
        })
    },
    //删除用户
    deleteUser({ commit }, { id }) {
        return new Promise((resolve, reject) => {
            delete_User(id)
                .then((response) => {
                    resolve(response.status);
                })
                .catch((error) => {
                    reject(error);
                })

        })
    },
    //增加用户
    increaseUser({ commit }, { user }) {
        return new Promise((resolve, reject) => {
            console.log(user);
            newUser(user)
                .then((response) => {
                    const { data } = response;
                    commit("ADD_USER", { data });
                    resolve(response.status);
                })
                .catch((error) => {
                    reject(error);
                })
        })
    },
    //修改用户
    reviseUser({ commit }, { user_id, user }) {
        return new Promise((resolve, reject) => {
            changeUser(user_id, user)
                .then((response) => {
                    resolve(response.status);
                })
                .catch((error) => {
                    reject(error);
                })
        })
    },
    //判断用户是否已存在
    is_Contains({ commit }, { data }) {
        return new Promise((resolve, reject) => {
            isContains(data)
                .then(response => {
                    const { data } = response;
                    resolve(data);
                })
                .catch(error => {
                    reject(error);
                })
        })
    }
}

export default {
    namespaced: true, //namespaced: true 的方式使其成为带命名空间的模块。保证在变量名一样的时候，添加一个父级名拼接。
    state,
    getters,
    actions,
    mutations
}