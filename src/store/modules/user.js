import { get_info, login_out, editSelfSafe } from "@/api/user";
const state = () => ({
  username: "",
  authority: "",
  avatar: "",
  real_name: "",
  phone_number: "",
  qq_number: "",
  description: "",
  class_name: "",
  common_name: "",
  department: "",
  wechat_number: "",
  gender: "",
  email: "",
  id: "",
  tokenState: ""
});
const mutations = {
  SET_USERINFO(state, userInfo) {
    state.username = userInfo.username;
    state.real_name = userInfo.real_name || "无";
    state.authority = userInfo.authority;
    state.avatar =
      userInfo.avatar ||
      "https://oss.sunxinao.cn/running-job/upload/841ab0c29389df0a12202a77da3ab15f";
    state.phone_number = userInfo.phone_number || "无";
    state.qq_number = userInfo.qq_number || "无";
    state.description = userInfo.description || "无";
    state.class_name = userInfo.class_name || "无";
    state.common_name = userInfo.common_name || "无";
    state.department = userInfo.department || "无";
    state.wechat_number = userInfo.wechat_number || "无";
    state.gender = userInfo.gender || "保密";
    state.email = userInfo.email || "无";
    state.id = userInfo.id;
  },
  REMOVE_TOKEN(state) {
    localStorage.removeItem("bearerToken");
    localStorage.removeItem("refreshToken");
    state.tokenState = 'Expired'
  },
  //修改头像
  UPDATE_AVATAR: (state, { avatar }) => {
    console.log(state);
    state.avatar = avatar;
  },
  tokenState(state, payload) {
    state.tokenState = payload;
  }
};
const actions = {
  get_info({ commit, state }) {
    return new Promise((resolve, reject) => {
      get_info()
        .then(response => {
          let {
            username,
            real_name,
            authority,
            avatar,
            phone_number,
            qq_number,
            description,
            class_name,
            common_name,
            department,
            wechat_number,
            gender,
            email,
            id
          } = response.data;
          commit("SET_USERINFO", {
            username,
            real_name,
            authority,
            avatar,
            phone_number,
            qq_number,
            description,
            class_name,
            common_name,
            department,
            wechat_number,
            gender,
            email,
            id
          });
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  login_out({ commit, state }) {
    return new Promise((resolve, reject) => {
      login_out()
        .then(response => {
          commit("SET_USERINFO", {});
          commit("REMOVE_TOKEN");
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  editSelfSafe({ commit, state }, playrod) {
    return new Promise((resolve, reject) => {
      const {
        captchaId,
        captcha,
        captcha_dest_addr,
        password,
        phoneNumber,
        email
      } = playrod;
      console.log(
        captchaId,
        captcha,
        captcha_dest_addr,
        password,
        phoneNumber,
        email
      );
      editSelfSafe(
        captchaId,
        captcha,
        captcha_dest_addr,
        password,
        phoneNumber,
        email
      )
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  }
};
export default {
  namespaced: true,
  state,
  mutations,
  actions
};
