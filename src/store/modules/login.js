import {
  reqpwdLogin,
  sms,
  email,
  registerUserByPhone,
  registerUserByEmail,
  forget_user_byphone,
  forget_user_byemail,
  login_bycaptcha
} from "@/api/login";

const state = () => ({
  account: "",
  password: "",
  username: "",
  captcha: "",
  captcha_id: ""
});

const getters = {};
//异步改变
const actions = {
  Login({ commit, state, dispatch }, userInfo) {
    const { username, password } = userInfo;
    return new Promise((resolve, reject) => {
      reqpwdLogin(username, password)
        .then(response => {
          localStorage.setItem("refreshToken", response.data.refresh_token);
          dispatch("user/get_info", {}, { root: true }).then(() => {
            resolve(response.data.scope);
          });
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  login_bycaptcha({ commit }, userInfo) {
    const { account, captcha, captcha_id } = userInfo;
    return new Promise((resolve, reject) => {
      login_bycaptcha(account, captcha, captcha_id)
        .then(response => {
          localStorage.setItem("refreshToken", response.data.refreshToken);
          dispatch("user/get_info", {}, { root: true }).then(() => {
            resolve(response.data.scope);
          });
        })
        .catch(error => {
          reject(error);
        });
    });
  },

  sms({ commit, state }, phone_number) {
    return new Promise((resolve, reject) => {
      sms(phone_number)
        .then(response => {
          const captcha_id = response.headers["captcha-id"];
          commit("UPDATE_CAPTCHA", { captcha_id });
          resolve("手机验证码发送成功");
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  email({ commit, state }, email_address) {
    return new Promise((resolve, reject) => {
      email(email_address)
        .then(response => {
          const captcha_id = response.headers["captcha-id"];
          commit("UPDATE_CAPTCHA", { captcha_id });
          resolve("邮箱验证码发送成功");
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  registerUserByPhone({ commit }, userInfo) {
    const { account, password, username, captcha, captcha_id } = userInfo;
    return new Promise((resolve, reject) => {
      registerUserByPhone(account, password, username, captcha, captcha_id)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  registerUserByEmail({ commit }, userInfo) {
    const { account, password, username, captcha, captcha_id } = userInfo;
    console.log(userInfo);
    return new Promise((resolve, reject) => {
      registerUserByEmail(account, password, username, captcha, captcha_id)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  forget_user_byphone({ commit }, userInfo) {
    const { account, password, captcha, captcha_id } = userInfo;
    console.log(userInfo);
    return new Promise((resolve, reject) => {
      forget_user_byphone(account, password, captcha, captcha_id)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  forget_user_byemail({ commit }, userInfo) {
    const { account, password, captcha, captcha_id } = userInfo;
    console.log(userInfo);
    return new Promise((resolve, reject) => {
      forget_user_byemail(account, password, captcha, captcha_id)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  }
};

//同步改变
const mutations = {
  UPDATE_CAPTCHA(state, { captcha_id }) {
    state.captcha_id = captcha_id;
  }
};
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
