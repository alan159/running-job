//引入api接口
import {
    init,
    init_history,
    passFirm,
    denyFirm
} from "@/api/admin/firm.js";
// import { reject, resolve } from "core-js/fn/promise";

//数据源
const state = {
    tableData: [], //表格数据
    page: { //page数据
        page_size: 0,
        page_number: 0,
        total_elements: 0,
    },
    firmMessage: {
        id: "",
        reason: "",
    },
}

const getters = {
    firmMessageId(state) {
        return state.firmMessage.id
    },
    firmMessageReason(state) {
        return state.firmMessage.reason
    },
    pageNumber(state) {
        return state.page.page_number
    },
    pageSize(state) {
        return state.page.page_size
    },
}

//同步请求
const mutations = {
    GET_DATA(state, { data }) {
        state.tableData = data.content;
        state.page = data.pageable;
        state.page.total_elements = data.total_elements;
    },
    //驳回申请
    DENY_FIRM(state, { id }) {
        state.firmMessage.id = id;
    },
}

//异步请求
const actions = {
    //获得商家申请数据
    getData({ commit }, { page, size }) {
        return new Promise((resolve, reject) => {
            init(page, size)
                .then((response) => {
                    let { data } = response;
                    commit('GET_DATA', { data });
                    resolve();
                })
                .catch((error) => {
                    reject(error);
                })
        })
    },
    //根据条件获得商家申请数据
    getHistoryData({ commit }, { isPass, page, size }) {
        return new Promise((resolve, reject) => {
            init_history(isPass, page, size)
                .then((response) => {
                    let { data } = response;
                    commit('GET_DATA', { data });
                    resolve();
                })
                .catch((error) => {
                    reject(error);
                })
        })
    },
    //通过申请
    pass({ commit }, { id }) {
        return new Promise((resolve, reject) => {
            passFirm(id)
                .then((response) => {
                    let { status } = response;
                    resolve(status);
                })
                .catch((error) => {
                    reject(error);
                })

        })
    },
    //驳回申请
    deny({ commit }, { id, reason }) {
        return new Promise((resolve, reject) => {
            denyFirm(id, reason).then((response) => {
                let { status } = response;
                resolve(status);
            }).catch((error) => {
                reject(error);
            })
        })
    },
}

export default {
    namespaced: true, //namespaced: true 的方式使其成为带命名空间的模块。保证在变量名一样的时候，添加一个父级名拼接。
    state,
    getters,
    actions,
    mutations
}