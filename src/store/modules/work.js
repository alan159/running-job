import { jobsInit, searchJobByTag, lookMyWork } from "@/api/workSpalce";
const state = () => ({
  currentPage: 1,
  pageSize: 10,
  totalElement: 0,
  workArray: [],
  tag: "全部分类"
});

const mutations = {
  SET_TOTAL(state, total) {
    state.totalElement = total;
  },
  SET_CURRENTPAGE(state, currentPage) {
    state.currentPage = currentPage;
  },
  SET_PAGESIZE(state, pageSize) {
    state.pageSize = pageSize;
  },
  SER_WORKARRAY(state, workArray) {
    state.workArray = workArray;
  },
  SET_TAG(state, tag) {
    state.tag = tag;
  }
};

const actions = {
  jobsInit({ commit, state }) {
    return new Promise((resolve, reject) => {
      jobsInit(state.currentPage, state.pageSize)
        .then(response => {
          commit("SET_TOTAL", response.data.page.total_elements);
          commit("SER_WORKARRAY", response.data._embedded.jobs);
          resolve(response.data);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  setCurrentPage({ commit, state, dispatch }, payload) {
    commit("SET_CURRENTPAGE", payload);
    if (state.tag === "全部分类") {
      dispatch("jobsInit");
    } else {
      dispatch("searchJobByTag");
    }
  },
  setPageSize({ commit, state, dispatch }, payload) {
    commit("SET_PAGESIZE", payload);
    if (state.tag === "全部分类") {
      dispatch("jobsInit");
    } else {
      dispatch("searchJobByTag");
    }
  },
  searchJobByTag({ commit, state, dispatch }) {
    return new Promise((resolve, reject) => {
      searchJobByTag(state.tag, state.currentPage, state.pageSize)
        .then(response => {
          let array = [];
          response.data.content.forEach(element => {
            let obj = {
              province: element.content["province"],
              city: element.content["city"],
              title: element.content["title"],
              id: element.content["id"],
              publisher: {
                username: element.content.publisher["username"]
              },
              tags_string: element.content["tags_string"]
            };
            // obj["province"] = element.content.province;
            // obj["city"] = element.content["city"];
            // obj["title"] = element.content["title"];
            // obj["username"] = element.content.publisher["username"];
            // obj["tags_string"] = element.content["tags_string"];
            array.unshift(obj);
          });
          commit("SER_WORKARRAY", array);
          commit("SET_TOTAL", response.data.total_elements);
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  lookMyWork({ commit, state }) {
    return new Promise((resolve, reject) => {
      lookMyWork(this.currentPage,this.pageSize)
        .then(response => {
          commit("SET_TOTAL", response.data.page.total_elements);
          commit("SER_WORKARRAY", response.data._embedded.jobs);
          resolve(response.data);
        })
        .catch(error => {
          reject(error);
        });
    });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
