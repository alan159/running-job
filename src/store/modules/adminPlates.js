import {
    getPlate,
    addPlate,
    delPlate
} from "@/api/admin/posts.js";

const state = {
    tableData: [],
    gridData: [],
    page: {},
    //模糊搜索表单
    ruleForm: {
        tipic: "",
    },
}

const getters = {}

const mutations = {
    GET_DATA(state, { data }) {
        state.tableData = data._embedded.plates;
        state.page = data.page;
    },
    CLEAR_TOPIC(state) {
        state.ruleForm.topic = "";
    },
}

const actions = {
    getData({ commit }, { page, size }) {
        return new Promise((resolve, reject) => {
            getPlate(page, size)
                .then(response => {
                    const { data } = response;
                    commit("GET_DATA", { data });
                    resolve();
                })
                .catch(error => {
                    reject(error);
                })
        })

    },
    //增加板块
    add_Plate({ commit }, { topic }) {
        return new Promise((resolve, reject) => {
            addPlate(topic)
                .then(response => {
                    const { status } = response;
                    resolve(status);
                })
                .catch(error => {
                    reject(error);
                })
        })
    },
    //删除板块
    del_Plate({ commit }, { plate_id }) {
        return new Promise((resolve, reject) => {
            delPlate(plate_id)
                .then(response => {
                    const { status } = response;
                    resolve(status);
                })
                .catch(error => {
                    reject(error);
                })
        })
    },
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}