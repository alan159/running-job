import {
    init,
    deleteJob,
    searchJob,
    init_condition
} from "@/api/admin/job.js";


const state = {
    tableData: [],
    ruleForm: {
        keyword: ""
    },
    page: {},
    url_data: {},
}

const getters = {

}

const mutations = {
    //如果无条件则通过此方式更新数据
    INIT_CONTROL(state, { response }) {
        state.url_data = response.config;
        let { data } = response;
        state.tableData = data._embedded.jobs;
        state.page = data.page;
    },
    //如果无条件则通过此方式更新数据
    SEARCH_CONTROL(state, { response }) {
        state.url_data = response.config;
        let { data } = response;
        state.tableData = [];
        for (const key of data.search_hits) {
            state.tableData.push(key.content);
        }
        state.page.total_elements = data.total_hits;
        state.page.number = state.url_data.params.page;
        state.page.size = state.url_data.params.size;
    },
    //改变每页数量时调用
    HANDLE_SIZE_CHANGE(state, { val }) {
        let count = state.page.total_elements;
        let page = state.page.number;
        page = page > Math.floor(count / val) ? Math.floor(count / val) : page;
        state.url_data.params.page = page;
        state.url_data.params.size = val;
    },
    //改变页数时调用
    HANDLE_CURRENT_CHANGE(state, { val }) {
        state.url_data.params.page = val - 1;
    },
}

const actions = {
    //初始化数据
    getData({ commit }, { page, size }) {
        return new Promise((resolve, reject) => {
            init(page, size)
                .then((response) => {
                    commit('INIT_CONTROL', { response });
                    resolve();
                })
                .catch((error) => {
                    reject(error);
                })
        })
    },
    //根据条件分页查询
    getDatePage({ commit }, { url, params }) {
        return new Promise((resolve, reject) => {
            init_condition(url, params)
                .then((response) => {
                    if (params.keyword) {
                        commit('SEARCH_CONTROL', { response });
                    } else {
                        commit('INIT_CONTROL', { response });
                    }
                    resolve();
                })
                .catch((error) => {
                    console.log(error);
                    reject(error);
                })

        })
    },
    //模糊查询工作
    search_Job({ commit }, { keyword, page, size }) {
        return new Promise((resolve, reject) => {
            searchJob(keyword, page, size)
                .then((response) => {
                    commit('SEARCH_CONTROL', { response });
                    resolve();
                })
                .catch((error) => {
                    reject(error);
                })
        })
    },
    //模糊查询工作
    removeJob({ commit }, { id }) {
        return new Promise((resolve, reject) => {
            deleteJob(id)
                .then((response) => {
                    resolve(response.status);
                })
                .catch((error) => {
                    reject(error);
                })
        })
    },
}

export default {
    namespaced: true, //namespaced: true 的方式使其成为带命名空间的模块。保证在变量名一样的时候，添加一个父级名拼接。
    state,
    getters,
    actions,
    mutations
}