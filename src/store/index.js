import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate";

//前台首页
import login from '@/store/modules/login'
import user from '@/store/modules/user'
import work from '@/store/modules/work';

//后台
import admin from '@/store/modules/adminUser'
import personCenter from '@/store/modules/personCenter'
import adminBusiness from '@/store/modules/adminBusiness'
import adminJob from '@/store/modules/adminJob'
import adminPosts from '@/store/modules/adminPosts'
import adminPlates from '@/store/modules/adminPlates'


import getters from './getters'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        login,
        user,
        work,
        admin,
        personCenter,
        adminBusiness,
        adminJob,
        adminPosts,
        adminPlates,
    },
    getters,
    plugins: [createPersistedState({
        storage: window.localStorage,
        reducer(val) {
            return {
                user: val.user,
                work: val.work
            }
        }
    })],
})