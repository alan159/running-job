const getters = {
  username: state => state.user.username,
  real_name: state => state.user.real_name,
  authority: state => state.user.authority,
  workCurrentPage: state => state.work.currentPage,
  workPageSize: state => state.work.pageSize,
  workTotalElement: state => state.work.totalElement,
  workArray: state => state.work.workArray,
  avatar: state => state.user.avatar,
  tag: state => state.work.tag,
  tokenState:state => state.user.tokenState
};
export default getters;
