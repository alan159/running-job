import Vue from 'vue'
import VueRouter from 'vue-router'

import admin from '@/router/admin.js'
import login from '@/router/login.js'
import user from "@/router/user.js"
import work from "@/router/work.js"

//解决点击当前路由警告
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location, onResolve, onReject) {
    if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject)
    return originalPush.call(this, location).catch(err => err)
}

Vue.use(VueRouter)

let routes = [{
    path: '/',
    name:'/',
    component: ()=> import("@/views/home/home"),
    meta: { title: '首页' }
}]

routes = routes.concat(admin);
routes = routes.concat(login);
routes = routes.concat(user);
routes = routes.concat(work);

const router = new VueRouter({
    routes
})

export default router