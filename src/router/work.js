let work = [
  {
    path: "/work",
    name: "work",
    meta: { title: "职位" },
    component: ()=> import("@/views/work/work")
  },
  {
    path: "/searchwork",
    name: "work",
    meta: { title: "职位" },
    component: ()=> import("@/views/work/searchWork")
  }
];

export default work