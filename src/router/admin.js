let admin = [{
    path: '/admin',
    name: "admin",
    meta: { title: '首页', content: '' },
    component: () =>
        import ('@/views/admin/admin.vue'),
    children: [{
            path: 'main',
            name: 'main',
            meta: { title: '首页', content: '' },
            component: () =>
                import ('@/components/adminMain/main.vue')
        },
        // {
        //     path: 'personCenter',
        //     name: 'personCenter',
        //     meta: { title: '个人中心', content: ['个人中心'] },
        //     component: () =>
        //         import ('@/components/personCenter/personCenter.vue')
        // },
        {
            path: 'adminPerson',
            name: 'adminPerson',
            meta: { title: '系统用户', content: ['用户管理', '系统用户'] },
            component: () =>
                import ('@/components/adminPerson/adminPerson.vue')
        }, {
            path: 'adminCommon',
            name: 'adminCommon',
            meta: { title: '普通用户', content: ['用户管理', '普通用户'] },
            component: () =>
                import ('@/components/adminPerson/adminCommon.vue')
        }, {
            path: 'adminBusiness',
            name: 'adminBusiness',
            meta: { title: '商家管理', content: ['用户管理', '商家管理'] },
            component: () =>
                import ('@/components/adminPerson/adminBusiness.vue')
        }, {
            path: 'administrators',
            name: 'administrators',
            meta: { title: '管理员', content: ['用户管理', '管理员'] },
            component: () =>
                import ('@/components/adminPerson/administrators.vue')
        }, {
            path: 'businessFirm',
            name: 'businessFirm',
            meta: { title: '正在申请', content: ['申请管理', '正在申请'] },
            component: () =>
                import ('@/components/businessFirm/businessFirm.vue')
        }, {
            path: 'successFirm',
            name: 'successFirm',
            meta: { title: '成功申请', content: ['申请管理', '成功申请'] },
            component: () =>
                import ('@/components/businessFirm/successFirm.vue')
        }, {
            path: 'failFirm',
            name: 'failFirm',
            meta: { title: '失败申请', content: ['申请管理', '失败申请'] },
            component: () =>
                import ('@/components/businessFirm/failFirm.vue')
        }, {
            path: 'adminJob',
            name: 'adminJob',
            meta: { title: '职位管理', content: ['职位管理'] },
            component: () =>
                import ('@/components/adminJob/adminJob.vue')
        }, {
            path: 'adminPlate',
            name: 'adminPlate',
            meta: { title: '板块管理', content: ['论坛管理', '板块管理'] },
            component: () =>
                import ('@/components/adminPlate/adminPlate.vue')
        }, {
            path: 'adminPosts',
            name: 'adminPosts',
            meta: { title: '帖子管理', content: ['论坛管理', '帖子管理'] },
            component: () =>
                import ('@/components/adminPlate/adminPosts.vue')
        }, { path: '', redirect: 'main' }
    ],
}, {
    path: '/webSocket',
    name: 'webSocket',
    component: () =>
        import ('@/components/webSocket/webSocket.vue')
}]
export default admin