let login = [
  {
    path: "/login",
    name: "login",
    meta: { title: "登录" },
    component: () => import("@/views/login/login.vue")
  },
  {
    path: "/register",
    name: "register",
    meta: { title: "注册" },
    component: () => import("@/views/login/register.vue")
  },
  {
    path: "/register_captcha",
    name: "register_captcha",
    meta: { title: "注册" },
    component: () => import("@/views/login/register_captcha.vue")
  },
  {
    path: "/forget_password",
    name: "forget_password",
    meta: { title: "忘记密码" },
    component: () => import("@/views/login/forget_password.vue")
  },
  {
    path: "/forget_password_captcha",
    name: "forget_password_captcha",
    meta: { title: "忘记密码" },
    component: () => import("@/views/login/forget_password_captcha.vue")
  }
];
export default login;
