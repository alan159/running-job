let user = [
  {
    path: "/userProfiles",
    name: "userProfiles",
    meta: { title: "个人档案" },
    component: () => import("@/views/user/userProfiles"),
    children: [
      {
        path: "personalDetails",
        name: "personalDetails",
        meta: { title: "个人信息" },
        component: () => import("@/components/user/personalDetails")
      },
      {
        path: "loginSecurity",
        name: "loginSecurity",
        meta: { title: "登录安全" },
        component: () => import("@/components/user/loginSecurity")
      },
      {
        path: "cooperationApplication",
        name: "cooperationApplication",
        meta: { title: "合作申请" },
        component: () => import("@/components/user/cooperationApplication")
      },
      {
        path: "release",
        name: "release",
        meta: { title: "发布职位" },
        component: () => import("@/components/merchant/release")
      },
      {
        path: "myRelease",
        name: "myRelease",
        meta: { title: "我的发布" },
        component: () => import("@/components/merchant/myRelease")
      },{
        path: "cooperationing",
        name: "cooperationing",
        meta: { title: "申请历史" },
        component: () => import("@/components/user/cooperationing")
      },
    ]
  }
];
export default user;
