//每页数量改变时触发
export function handleSizeChange(val) {
    let count = this.$store.state.admin.page.total_elements;
    let page = this.$store.state.admin.page.number;
    page = page > Math.floor(count / val) ? Math.floor(count / val) : page;
    this.$store.dispatch("admin/changePage", {
        url: this.$store.state.admin.url_data.url,
        page: page,
        size: val,
        key: this.$store.state.admin.url_data.key,
        val: this.$store.state.admin.url_data.val,
    });
}
//当前页数改变时触发
export function handleCurrentChange(val) {
    this.changePage({
        url: this.$store.state.admin.url_data.url,
        page: val - 1,
        size: this.$store.state.admin.page.size,
        key: this.$store.state.admin.url_data.key,
        val: this.$store.state.admin.url_data.val,
    });
}
//删除用户
export function delUser(id, name) {
    this.$confirm(`此操作将永久删除${name}, 是否继续?`, "警告", {
            confirmButtonText: "确定",
            cancelButtonText: "取消",
            type: "warning",
        })
        .then(async() => {
            let status = await this["deleteUser"]({ id });
            if (status == 200) {
                this.$message({
                    type: "success",
                    message: "删除成功!",
                });
                setTimeout(() => {
                    this.refreshData();
                }, 500);
            }
        })
        .catch((error) => {
            this.$message({
                type: "info",
                message: error,
            });
        });
}
//添加用户
export async function addUser(formName) {
    let user = this.ruleForm;
    for (const key in user) {
        if (user[key] == "") user[key] = null;
    }
    if (user.password == null) {
        user.password = "111111";
    }
    this.$refs[formName].validate(async(valid) => {
        if (valid) {
            let status = await this["increaseUser"]({ user });
            if (status == 200) {
                this.$emit("update:loading", true);
                setTimeout(() => {
                    this.$emit("update:visible", false);
                    this.$emit("update:loading", false);
                    this.$message({
                        type: "success",
                        message: "添加成功!",
                    });
                    setTimeout(() => {
                        this.refreshData();
                    }, 500);
                }, 3000);
            }
        } else {
            console.log("error submit!!");
            return false;
        }
    });
}
//获取用户信息并渲染
export function gain_User(id, visible) {
    this["gainUser"]({ id });
    this.$emit("update:visible", true);
}
//修改用户
export async function changeUser(formName) {
    let user = this.$store.state.admin.change_user;
    let user_id = user.id;
    this.$refs[formName].validate(async(valid) => {
        if (valid) {
            this.$emit("update:loading", true);
            let status = await this["reviseUser"]({ user_id, user });
            if (status == 200) {
                setTimeout(() => {
                    this.$emit("update:visible", false);
                    this.$emit("update:loading", false);
                    this.$message({
                        type: "success",
                        message: "修改成功!",
                    });
                    setTimeout(() => {
                        this.refreshData();
                    }, 500);
                }, 2000);
            }
        } else {
            console.log("error submit!!");
            return false;
        }
    });
}
//重置表单
export function resetForm() {
    this.$store.commit("admin/CLEAR_USER", { key: "", val: "" });
    this.getData({ page: 0, size: 10 });
}
//取消修改用户
export function clearUser(formName) {
    this.$emit("update:visible", false);
    this.$refs[formName].resetFields();
}
//检查手机号
export function isCellPhone(val) {
    if (!/^1(3|4|5|6|7|8)\d{9}$/.test(val)) {
        return false;
    } else {
        return true;
    }
}
//刷新数据
export function refreshData(page, size) {
    this.changePage({
        url: this.$store.state.admin.url_data.url,
        page: page || this.$store.state.admin.page.numbe,
        size: size || this.$store.state.admin.page.size,
        key: this.$store.state.admin.url_data.key,
        val: this.$store.state.admin.url_data.val,
    });
}

//判断用户名是否存在
export async function is_Contains(key, value) {
    let data = {};
    data[key] = value;
    const response = await this.$store.dispatch("admin/is_Contains", {
        data
    });
    return response;
}